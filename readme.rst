Human Activity Recognition
==========================

To illustrate the use of `PySeqLab package <https://bitbucket.org/A_2/pyseqlab>`__ in training a ``modes of locomotion classifier`` and ``gestures movements classifier``
using the `OPPORTUNITY activity recognition dataset <https://archive.ics.uci.edu/ml/datasets/OPPORTUNITY+Activity+Recognition#>`__.
Navigate to the ``tutorials`` folder to explore the notebooks for building and training ``classifiers``. A rendered html version of the tutorial is available on `PySeqLab applications page <http://pyseqlab.readthedocs.io/en/latest/applications.html>`__.